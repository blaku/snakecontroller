﻿namespace SNAKE
{
	class AX12A : Servo
	{
		//Registers in EEPROM												Address(Hex)	Description										Access	Initial Value (Hex)
		public const byte register_Model_Number_L = 0;                      // (0X00)	Lowest byte of model number							R		12 (0X0C)
		public const byte register_Model_Number_H = 1;                      // (0X01)	Highest byte of model number						R		0 (0X00)
		public const byte register_Version_of_Firmware = 2;                 // (0X02)	Information on the version of firmware				R		-
		public const byte register_ID = 3;                                  // (0X03)	ID of Dynamixel										RW		1 (0X01)
		public const byte register_Baud_Rate = 4;                           // (0X04)	Baud Rate of Dynamixel								RW		1 (0X01)
		public const byte register_Return_Delay_Time = 5;                   // (0X05)	Return Delay Time									RW		250 (0XFA)
		public const byte register_CW_Angle_Limit_L = 6;                    // (0X06)	Lowest byte of clockwise Angle Limit				RW		0 (0X00)
		public const byte register_CW_Angle_Limit_H = 7;                    // (0X07)	Highest byte of clockwise Angle Limit				RW		0 (0X00)
		public const byte register_CCW_Angle_Limit_L = 8;                   // (0X08)	Lowest byte of counterclockwise Angle Limit			RW		255 (0XFF)
		public const byte register_CCW_Angle_Limit_H = 9;                   // (0X09)	Highest byte of counterclockwise Angle Limit		RW		3 (0X03)
		public const byte register_The_Highest_Limit_Temperature = 11;   // (0X0B)	Internal Limit Temperature							RW		70 (0X46)
		public const byte register_The_Lowest_Limit_Voltage = 12;          // (0X0C)	Lowest Limit Voltage								RW		60 (0X3C)
		public const byte register_The_Highest_Limit_Voltage = 13;       // (0X0D)	Highest Limit Voltage								RW		140 (0XBE)
		public const byte register_Max_Torque_L = 14;                       // (0X0E)	Lowest byte of Max.Torque							RW		255 (0XFF)
		public const byte register_Max_Torque_H = 15;                       // (0X0F)	Highest byte of Max.Torque							RW		3 (0X03)
		public const byte register_Status_Return_Level = 16;                // (0X10)	Status Return Level									RW		2 (0X02)
		public const byte register_Alarm_LED = 17;                          // (0X11)	LED for Alarm										RW		36(0x24)
		public const byte register_Alarm_Shutdown = 18;                    // (0X12)	Shutdown for Alarm									RW		36(0x24)

		//Registers in RAM
		public const byte register_Torque_Enable = 24;                      // (0X18)	Torque On/Off										RW		0(0X00)
		public const byte register_LED = 25;                                // (0X19)	LED On/Off											RW		0(0X00)
		public const byte register_CW_Compliance_Margin = 26;               // (0X1A)	CW Compliance margin                                RW		1 (0X01)
		public const byte register_CCW_Compliance_Margin = 27;              // (0X1B)	CCW Compliance margin                               RW		1 (0X01)
		public const byte register_CW_Compliance_Slope = 28;                // (0X1C)	CW Compliance slope                                 RW		32 (0X20)
		public const byte register_CCW_Compliance_Slope = 29;               // (0X1D)	CCW Compliance slope                                RW		32 (0X20)
		public const byte register_Goal_Position_L = 30;                    // (0X1E)	Lowest byte of Goal Position						RW		-
		public const byte register_Goal_Position_H = 31;                    // (0X1F)	Highest byte of Goal Position						RW		-
		public const byte register_Moving_Speed_L = 32;                     // (0X20)	Lowest byte of Moving Speed(Moving Velocity)		RW		-
		public const byte register_Moving_Speed_H = 33;                     // (0X21)	Highest byte of Moving Speed(Moving Velocity)		RW		-
		public const byte register_Torque_Limit_L = 34;                     // (0X22)	Lowest byte of Torque Limit(Goal Torque)			RW		ADD14
		public const byte register_Torque_Limit_H = 35;                     // (0X23)	Highest byte of Torque Limit(Goal Torque)			RW		ADD15
		public const byte register_Present_Position_L = 36;                 // (0X24)	Lowest byte of Current Position(Present Velocity)	R		-
		public const byte register_Present_Position_H = 37;                 // (0X25)	Highest byte of Current Position(Present Velocity)	R		-
		public const byte register_Present_Speed_L = 38;                    // (0X26)	Lowest byte of Current Speed						R		-
		public const byte register_Present_Speed_H = 39;                    // (0X27)	Highest byte of Current Speed						R		-
		public const byte register_Present_Load_L = 40;                     // (0X28)	Lowest byte of Current Load							R		-
		public const byte register_Present_Load_H = 41;                     // (0X29)	Highest byte of Current Load						R		-					   
		public const byte register_Present_Voltage = 42;                    // (0X2A)	Current Voltage										R		-
		public const byte register_Present_Temperature = 43;                 // (0X2B)	Current Temperature									R		-
		public const byte register_Registered = 44;                         // (0X2C)	Means if Instruction is registered					R		0 (0X00)
		public const byte register_Moving = 46;                             // (0X2E)	Means if there is any movement						R		0 (0X00)
		public const byte register_Lock = 47;                               // (0X2F)	Locking EEPROM										RW		0 (0X00)
		public const byte register_Punch_L = 48;                            // (0X30)	Lowest byte of Punch								RW		32 (0X20)
		public const byte register_Punch_H = 49;                            // (0X31)	Highest byte of Punch								RW		0 (0X00)
		
		public const string servoType = "AX-12A";
		public const int protocolVersion = 1;

		/// <summary>
		/// A Dynamixel AX-12A servo
		/// </summary>
		/// <param name="servoID">[0..253]</param>
		public AX12A(byte servoID, DynamixelController controller, MainWindow messageHandler)
			: base(servoID, controller, messageHandler, protocolVersion, servoType)
		{
			//No init necessary
		}
	}
}
