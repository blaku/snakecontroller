﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// This class is a data object.
// The goal of this class is to decompose a message then cast it to the right command. Messages are in JSON, they will be decomposed and assigned to values
// automatically within this class. After which we can call the right values and identify them accordingly. Some values will remain null depending on packet type

// Type: is type of command, suchcommunication, control, feedback..etc.
// command: actual command to carry
// origin: sender
// dst: destination
// details: other parameters, such as angle values, controlled servos or others.

// TODO: add further members to this packet.
class AnonPacket
{

	public string type="", command="", origin="", dst="", detail="";







}