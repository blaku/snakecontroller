﻿using System;
using System.Collections.Generic;
using dynamixel_sdk;

namespace SNAKE
{
	class DynamixelController
	{
		// Communication Result
		public const int COMM_SUCCESS = 0;       // tx or rx packet communication success
		public const int COMM_PORT_BUSY = -1000;   // Port is busy (in use)
		public const int COMM_TX_FAIL = -1001;// Failed to transmit instruction packet
		public const int COMM_RX_FAIL = -1002;// Failed to get status packet
		public const int COMM_TX_ERROR = -2000;// Incorrect instruction packet
		public const int COMM_RX_WAITING = -3000;// Now recieving status packet
		public const int COMM_RX_TIMEOUT = -3001; // There is no status packet
		public const int COMM_RX_CORRUPT = -3002;  // Incorrect status packet
		public const int COMM_NOT_AVAILABLE = -9000;   // Not supported by protocol

		public const int PROTOCOL1 = 1;
		public const int PROTOCOL2 = 2;
		public const int TORQUE_ENABLE = 1;                   // Value for enabling the torque
		public const int TORQUE_DISABLE = 0;                  // Value for disabling the torque

		//the servo ID of the selected servo to check the voltage and used protocol from
		byte selectedVoltageTimerServoID;
		int selectedVoltageTimerServoProtocol;

		#region "Properties"
		public string Port { get; } // e.g. "COM3"
		public int ProtocolVersion { get; } = 1;//TODO this will not be needed because each function should read the protocol version?
		public bool IsConnected { get; set; } = false;
		public MainWindow MessageHandler { get; set; }// a reference to the main GUI window
		private int Port_num { get; set; }

		private int baudRate = 1000000;
		/// <summary>
		/// Allowed baud rates (BPS): 9600, 19200, 57600, 115200, 200000, 250000, 400000, 500000, 1000000
		/// </summary>
		public int BaudRate
		{
			get
			{
				return baudRate;
			}
			set
			{
				List<int> allowedBaudRates = new List<int> { 9600, 19200, 57600, 115200, 200000, 250000, 400000, 500000, 1000000 };
				if (!allowedBaudRates.Contains(value))
				{
					MessageHandler.PostError("Error 015: Baud rate not allowed");
					return;
				}
				baudRate = value;
			}
		}
		#endregion

		public DynamixelController(string port, int baudRate, MainWindow messageHandler)
		{
			MessageHandler = messageHandler;
			Port = port;
			BaudRate = baudRate;
		}

		#region "Public functions"
		public bool Connect()
		{
			try
			{
				Port_num = dynamixel.portHandler(Port);
				dynamixel.packetHandler();// Initialize PacketHandler Structs			
				if (dynamixel.openPort(Port_num))
				{
					MessageHandler.Log("Port opened");
				}
				else
				{
					MessageHandler.PostError("Error 020: Failed to open port");
					return false;
				}

				// Set port baud rate
				if (dynamixel.setBaudRate(Port_num, BaudRate))
				{
					MessageHandler.Log("Baud rate changed");
				}
				else
				{
					MessageHandler.PostError("Error 021: Failed to change baud rate");
					return false;
				}
				IsConnected = true;
				return true;
			}
			catch
			{
				MessageHandler.PostError("Error 040: Failed to connect");
				return false;
			}
		}

		public void Disconnect()
		{
			dynamixel.closePort(Port_num);
			MessageHandler.Log("Port closed");
			IsConnected = false;
		}

		public bool EnableTorque(Servo servo)
		{
			//MessageHandler.Log("Enabling torque on servo " + servo.ID);
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Torque_Enable;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Torque_Enable;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Torque_Enable;
			}
			dynamixel.write1ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, TORQUE_ENABLE);
			return CheckResults();
		}

		public bool DisableTorque(Servo servo)
		{
			//MessageHandler.Log("Disabling torque on servo " + servo.ID);
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Torque_Enable;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Torque_Enable;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Torque_Enable;
			}
			dynamixel.write1ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, TORQUE_DISABLE);
			return CheckResults();
		}

		public bool Ping(byte ID, int protocol)
		{
			if (protocol == PROTOCOL1 && (ID < 0 || ID > 253))
			{
				MessageHandler.PostError("Error 027: ID not pingable");
				return false;
			}
			if (protocol == PROTOCOL2 && (ID < 0 || ID > 252))
			{
				MessageHandler.PostError("Error 028: ID not pingable");
				return false;
			}
			MessageHandler.Log("Pinging servo with ID " + ID);
			dynamixel.ping(Port_num, protocol, ID);
			if (CheckResults())
			{
				MessageHandler.Log("Pong!");
				return true;
			}
			else return false;
		}

		public List<byte> PingAll(int protocol)
		{
			MessageHandler.Log("Pinging all possible servos on protocol " + protocol + " at baud rate " + BaudRate);
			List<byte> IDs = new List<byte>();
			byte upperLimit = 0;
			if (protocol == PROTOCOL1)
				upperLimit = 253;
			else if (protocol == PROTOCOL2)
				upperLimit = 252;
			else
			{
				MessageHandler.PostError("Error 029: Bad protocol version");
				return null;
			}
			for (byte i = 0; i < upperLimit; i++)
			{
				dynamixel.ping(Port_num, protocol, i);
				if (CheckResultsQuietly())
				{
					MessageHandler.Log("ID " + i + " OK");
					IDs.Add(i);
				}
				else
					MessageHandler.Log("ID " + i + " Not OK");
			}
			return IDs;
		}

		public bool ReadAllAngles(Robot robot, ref List<double> anglesToReturn)
		{
			anglesToReturn.Clear();
			double a = 0;
			bool succeeededToReadAll = true;
			foreach (Servo s in robot.AllServos)
			{
				if (!s.ReadAngle(ref a))
				{
					MessageHandler.PostError("Error 030: Failed to read angle of servo with ID " + s.ID);
					a = 0;
					anglesToReturn.Add(a);
					succeeededToReadAll = false;
				}
				else
				{
					MessageHandler.Log("Servo " + s.ID + " at angle " + Math.Round(a, 2) + " degrees");
				}
			}
			return succeeededToReadAll;
		}

		public bool ReadAnglesFromAX12As(List<byte> IDs, ref List<double> anglesToReturn)
		{
			List<Servo> servos = new List<Servo>();
			foreach (byte id in IDs)
			{
				servos.Add(new AX12A(id, this, MessageHandler));
			}
			Robot r = new Robot(servos, MessageHandler);
			if (!ReadAllAngles(r, ref anglesToReturn))
			{
				MessageHandler.PostError("Error 024: Failed to read multiple AX-12A angles");
				return false;
			}
			return true;
		}

		public bool TurnToAngle(Servo servo, double angle)
		{
			ushort ang = AngleDegreesTo_ushort(angle, servo.GetType(), MessageHandler);
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Goal_Position_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Goal_Position_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Goal_Position_L;
			}

			MessageHandler.Log("Servo " + servo.ID + " is turning to " + Angle_ushortToDegrees(ang, servo.GetType(), MessageHandler) + " degrees");
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, ang);
			return CheckResults();
		}

		/// <summary>
		/// The maximum amount of torque before the Overload Error is thrown and Alarm Shutdown is triggered by default
		/// </summary>
		/// <param name="servo"></param>
		/// <param name="torquePercentage"></param>
		/// <returns></returns>
		public bool WriteMaxTorque(Servo servo, double torquePercentage)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Max_Torque_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Max_Torque_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Max_Torque_L;
			}
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, PercentageToTorque(torquePercentage));
			return CheckResults();
		}

		/// <summary>
		/// The torque limit is reset to Max Torque when power is cylced. The torque limit is set to 0 when Alarm Shutdown is triggered. Set it to something else to return the servo from the shutdown. 
		/// </summary>
		/// <param name="servo"></param>
		/// <param name="torquePercentage"></param>
		/// <returns></returns>
		public bool WriteTorqueLimit(Servo servo, double torquePercentage)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Torque_Limit_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Torque_Limit_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Goal_Torque_L;
			}
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, PercentageToTorque(torquePercentage));
			return CheckResults();
		}

		/// <summary>
		/// Write Moving Speed (Goal Velocity) in % of maximum
		/// </summary>
		/// <param name="servo"></param>
		/// <param name="speed">% of maximum</param>
		public bool WriteSpeed(Servo servo, double speedPercentage)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Moving_Speed_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Moving_Speed_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Goal_Velocity_L;
			}
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, PercentageToSpeed_ushort(speedPercentage));
			return CheckResults();
		}

        /* This method is duplicate with the one below it... not sure which one is correct or why they are duplicates
		/// <summary>
		/// Read Present Speed in % of maximum. Negative if CCW, positive if CW
		/// </summary>
		/// <param name="speed">% of maximum, negative if CCW, positive if CW</param>
		public bool ReadSpeed(Servo servo, ref double speedPercentage)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Present_Speed_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Present_Speed_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Present_Speed_L;
			}
			speedPercentage = Speed_ushortToPercentage(dynamixel.read2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register));
			return CheckResults();
		}

    */

		/// <summary>
		/// Read Present Speed in % of maximum. Negative if CCW, positive if CW
		/// </summary>
		/// <param name="speed">% of maximum, negative if CCW, positive if CW</param>
		public bool ReadSpeed(Servo servo, ref double speedPercentage)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Present_Speed_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Present_Speed_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Present_Speed_L;
			}
			speedPercentage = Speed_ushortToPercentage(dynamixel.read2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register));
			return CheckResults();
		}



		public bool WriteCW_AngleLimit(Servo servo, double CW_AngleLimit)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_CW_Angle_Limit_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_CW_Angle_Limit_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_CW_Angle_Limit_L;
			}
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, AngleDegreesTo_ushort(CW_AngleLimit, servo.GetType(), MessageHandler));
			return CheckResults();
		}

		public bool WriteCCW_AngleLimit(Servo servo, double CCW_AngleLimit)
		{
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_CCW_Angle_Limit_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_CCW_Angle_Limit_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_CCW_Angle_Limit_L;
			}
			dynamixel.write2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register, AngleDegreesTo_ushort(CCW_AngleLimit, servo.GetType(), MessageHandler));
			return CheckResults();
		}

		public bool ReadRegisters(byte ID, int protocolVersion, ushort address, int bytesToRead, ref List<byte> resultToReturn)
		{
			resultToReturn.Clear();
			bool suuceededReadingAll = true;
			for (ushort i = 0; i < bytesToRead; i++)
			{
				resultToReturn.Add(dynamixel.read1ByteTxRx(Port_num, protocolVersion, ID, (ushort)(address + i)));
				if (!CheckResultsQuietly())
					suuceededReadingAll = false;
			}
			return suuceededReadingAll;
		}

		/// <summary>
		/// Reads an AX12-A and returns the voltage in units of actual volts
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="protocolVersion"></param>
		/// <returns>Voltage in volt</returns>
		public double ReadVoltage(byte ID, int protocolVersion)
		{
			try
			{
				return dynamixel.read1ByteTxRx(Port_num, protocolVersion, ID, AX12A.register_Present_Voltage) / 10.0;//same register (42) on AX and MX, but 45 on XL
			}
			catch
			{
				MessageHandler.PostError("Error 041: Failed to update voltage");
				return 0;
			}
		}

		public void CheckVoltageTimer(byte servoID, int protocol, int hours, int minutes, int seconds)
		{
			this.selectedVoltageTimerServoID = servoID;
			this.selectedVoltageTimerServoProtocol = protocol;

			var dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
			dispatcherTimer.Tick += new EventHandler(CheckVoltageTimer);
			dispatcherTimer.Interval = new TimeSpan(hours, minutes, seconds);
			dispatcherTimer.Start();
		}

		public bool ReadAngle(Servo servo, ref double angleReturned)
		{
			MessageHandler.Log("Reading angle of servo " + servo.ID);
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Present_Position_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Present_Position_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Present_Position_L;
			}
			ushort ang = dynamixel.read2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register);
			angleReturned = Angle_ushortToDegrees(ang, servo.GetType(), MessageHandler);
			return CheckResults();
		}

		public bool ReadLoad(Servo servo, ref double loadReturned)
		{
			MessageHandler.Log("Reading load of servo " + servo.ID);
			ushort register = 0;
			if (servo.GetType() == typeof(AX12A))
			{
				register = AX12A.register_Present_Load_L;
			}
			else if (servo.GetType() == typeof(MX64AT))
			{
				register = MX64AT.register_Present_Load_L;
			}
			else if (servo.GetType() == typeof(XL320))
			{
				register = XL320.register_Present_Load_L;
			}
			ushort load = dynamixel.read2ByteTxRx(Port_num, servo.ProtocolVersion, servo.ID, register);
			loadReturned = Load_ushortToSignedPercentage(load);
			return CheckResults();
		}

		/// <summary>
		/// Writes CW and CCW angle limits to multiple servos
		/// </summary>
		/// <param name="servoIDs"></param>
		/// <param name="anglesCW"></param>
		/// <param name="anglesCCW"></param>
		/// <returns></returns>
		public bool WriteMultipleAngleLimits(List<Servo> servos, List<double> anglesCW, List<double> anglesCCW)
		{
			if (!(servos.Count == anglesCW.Count && servos.Count == anglesCCW.Count))
			{
				MessageHandler.PostError("Error 051: Unequeal count of angles and servos");
				return false;
			}
			bool success = true;
			int i = 0;
			foreach (var s in servos)
			{
				if (!(WriteCW_AngleLimit(s, anglesCW[i]) && WriteCCW_AngleLimit(s, anglesCCW[i])))
				{
					success = false;
					MessageHandler.Log("Failed to write angle limits for servo " + s.ID);
					continue;
				}
				MessageHandler.Log("Wrote angle limits for servo " + s.ID);
				i++;
			}
			return success;
		}

		//public bool syncRead(List<byte> IDs, byte register, ref List<double> returnValues)//TODO
		//{
		//	;
		//	//if()
		//	//return false;
		//	//else
		//	return true;
		//}
		#endregion

		#region "Private functions"
		public static int Map(double val, int in_min, int in_max, int out_min, int out_max)//TODO should we make Map and Constrain work with doubles to increase precision?
		{
			return (int)Math.Round((val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
		}

		public static int Constrain(int value, int min, int max)
		{
			if (value > max)
				return max;
			else if (value < min)
				return min;
			else return value;
		}

		/// <summary>
		/// MX servos have double the resolution
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="servoType"></param>
		/// <param name="messageHandler"></param>
		/// <returns></returns>
		public static ushort AngleDegreesTo_ushort(double angle, Type servoType, MainWindow messageHandler)
		{
			if (servoType == typeof(AX12A))
			{
				return (ushort)Constrain(Map(angle, -150, 150, 0, 1023), 0, 1023);
			}
			else if (servoType == typeof(MX64AT))
			{
				return (ushort)Constrain(Map(angle, -180, 180, 0, 4095), 0, 4095);
			}
			else if (servoType == typeof(XL320))
			{
				return (ushort)Constrain(Map(angle, -150, 150, 0, 1023), 0, 1023);
			}
			else
			{
				messageHandler.PostError("Error 045: Bad servo type");
				return (ushort)Constrain(Map(angle, -150, 150, 0, 1023), 0, 1023);
			}
		}

		/// <summary>
		/// MX servos have double the resolution
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="servoType"></param>
		/// <param name="messageHandler"></param>
		/// <returns></returns>
		public static double Angle_ushortToDegrees(ushort angle, Type servoType, MainWindow messageHandler)
		{
			if (servoType == typeof(AX12A))
			{
				return Constrain(Map(angle, 0, 1023, -150, 150), -150, 150);
			}
			else if (servoType == typeof(MX64AT))
			{
				return Constrain(Map(angle, 0, 4095, -180, 180), -180, 180);
			}
			else if (servoType == typeof(XL320))
			{
				return Constrain(Map(angle, 0, 1023, -150, 150), -150, 150);
			}
			else
			{
				messageHandler.PostError("Error 046: Bad servo type");
				return (ushort)Constrain(Map(angle, -150, 150, 0, 1023), 0, 1023);
			}
		}

		private ushort PercentageToTorque(double torquePercentage)
		{
			return (ushort)Constrain(Map(torquePercentage, 0, 100, 0, 1023), 0, 1023);
		}

		/// <summary>
		/// For joint mode only. Convert a percentage of maximum speed into a ushort
		/// </summary>
		/// <param name="speedPercentage">[0..100] %</param>
		/// <returns></returns>
		private ushort PercentageToSpeed_ushort(double speedPercentage)
		{
			return (ushort)Constrain(Map(speedPercentage, 0, 100, 0, 1023), 0, 1023);
		}

		private double Speed_ushortToPercentage(ushort speed)
		{
			return Constrain(Map(speed, 0, 2047, -100, 100), -100, 100);
		}

		private double Load_ushortToSignedPercentage(ushort load)
		{
			return (double)Constrain(Map(load, 0, 2047, -100, 100), -100, 100);
		}

		private bool CheckResults()
		{
			int dxl_comm_result = COMM_TX_FAIL;
			if ((dxl_comm_result = dynamixel.getLastTxRxResult(Port_num, ProtocolVersion)) != COMM_SUCCESS)
			{
				//dynamixel.printTxRxResult(ProtocolVersion, dxl_comm_result);//TODO where does the printf in the DLL print?
				MessageHandler.PostError("Error code: " + DynamixelErrorIntToString(dxl_comm_result));
				return false;
			}
			return true;
		}

		private bool CheckResultsQuietly()
		{
			int dxl_comm_result = COMM_TX_FAIL;
			if ((dxl_comm_result = dynamixel.getLastTxRxResult(Port_num, ProtocolVersion)) != COMM_SUCCESS)
			{
				return false;
			}
			return true;
		}

		private string DynamixelErrorIntToString(int result)
		{
			switch (result)
			{
				case COMM_SUCCESS:
					return "[TxRxResult] Communication successful";
				case COMM_PORT_BUSY:
					return "[TxRxResult] Port is busy!";
				case COMM_TX_FAIL:
					return "[TxRxResult] Failed to transmit instruction packet";
				case COMM_RX_FAIL:
					return "[TxRxResult] Failed to get status packet from device";
				case COMM_TX_ERROR:
					return "[TxRxResult] Incorrect instruction packet";
				case COMM_RX_WAITING:
					return "[TxRxResult] Now recieving status packet";
				case COMM_RX_TIMEOUT:
					return "[TxRxResult] There was no status packet";
				case COMM_RX_CORRUPT:
					return "[TxRxResult] Incorrect status packet";
				case COMM_NOT_AVAILABLE:
					return "[TxRxResult] Protocol does not support this function";
				default:
					return "[TxRxResult] Unknown result";
			}
		}

		//This method is called after public void CheckVoltageTimer
		private void CheckVoltageTimer(object sender, EventArgs e)
		{
			double voltage = ReadVoltage(this.selectedVoltageTimerServoID, this.selectedVoltageTimerServoProtocol);
			MessageHandler.Log("Voltage: " + voltage);
			//TODO update a label with the voltage instead of printing to textbox
		}

        /* this method is duplicate too
        private double Speed_ushortToPercentage(ushort speed)
		{
			return Constrain(Map(speed, 0, 2047, -100, 100), -100, 100);
		}
        */
        #endregion
    }

}
