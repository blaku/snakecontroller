﻿//This class is not used any more, but the errors are listed here for convenience. 

/*
"Error 001: Speed out of range. " + s + " %"
"Error 002: Number of movement patameters don't match"
"Error 003: Bad angle" + a + " degrees"
"Error 004: Failed to read load for compliant step"
"Error 005: Failed to read angle for compliant step"
"Error 006: Failed to take compliant step CCW"
"Error 007: Failed to take compliant step CW"
"Error 008: Failed to read present loads")
"Error 009: Servo count doesn't match target angle count"
"Error 010: Servo count doesn't match speed count"
"Error 011: Bad servo ID index, target angle index or speed index for moving in this robot"
"Error 012: Failed to turn a servo in the movement"
"Error 013: Bad servo index to check if moving"
"Error 014: CW_AngleLimit out of range"
"Error 015: Baud rate not allowed"
"Error 016: ComplianceLoadThreshold not in %"
"Error 017: angularError out of range"
"Error 018: Bad protocol version"
"Error 019: CCW_AngleLimit out of range"
"Error 020: Failed to open port"
"Error 021: Failed to change baud rate"
"Error 022: AngleErrorThreshold out of range"
"Error 023: Failed to go to angle"
"Error 024: Failed to read multiple AX-12A angles"
"Error 025: Failed to set CW angle limit"
"Error 026: Failed to set CCW angle limit"
"Error 027: ID not pingable"
"Error 028: ID not pingable"
"Error 029: Bad protocol version"
"Error 030: Failed to read angle of servo with ID " + s.ID
"Error 031: Failed to read angle"
"Error 032: Failed to write max torque"
"Error 033: Failed to write torque limit for servo "+ this.ID
"Error 034: Failed to enable torque"
"Error 035: Failed to disable torque"
"Error 036: Failed to write speed"
"Error 037: Failed to set ID"
"Error 038: Failed to read load"
"Error 039: Failed to read speed"
"Error 040: Failed to connect"
"Error 041: Bad input in ComboBoxCommands"
"Error 042: Bad servo type"
"Error 043: Bad input"
"Error 044: Bad input"
"Error 045: Bad servo type"
"Error 046: Bad servo type"
"Error 047: Failed to ping servo " + this.ID
"Error 048: Failed to lock in place"
"Error 049: No such part to lock"
"Error 050: Failed to lock in place"
"Error 051: Counts of movements and movementIntervals don't match"
"Error 052: Failed to check if moving"
"Error 053: Failed to play a movement in the sequence"
"Error 054: Servo count doesn't match target angle count"
"Error 055: Servo count doesn't match speed count"
"Error 056: Failed to turn a servo in the movement"
"Error 057: Bad servo ID index, target angle index or speed index for moving in this robot"
"Error 058: Failed to check if moving"
"Error 059: Bad servo index to check if moving"
"Error 060: Failed to play a movement in the sequence"


"Warning 001: Compliant mode already active"
"Warning 002: Compliant mode already inactive"

MessageHandler.PostError("Error 000: Description");
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNAKE
{
	class Error
	{
		//public static void Post(String e)
		//{
		//	//Console.WriteLine(e);
		//	//MainWindow.postError(e);
		//}
	}
}
