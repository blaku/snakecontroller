﻿using System.IO;

namespace SNAKE
{
	class FileRW
	{
		private MainWindow Win { get;set; }


		public FileRW(MainWindow window)
		{
			Win = window;
		}

		public void Write2File(string fileName,string path, string values)
		{
			if (fileName==null || fileName.Equals(""))
			{ fileName = "temp"; }

			if (path.Equals("") || path == null)
			{ path = @"c:\temp\" + fileName + ".json"; }

			try
			{
				File.WriteAllText(path, values);
			}
			catch (IOException e)
			{
				Win.Log("Error: "+e.Data.ToString());

			}

		}

		public string ReadFromFile(string fileName, string path)
		{
			try
			{
				return File.ReadAllText(path + fileName);
			}
			catch (IOException e)
			{ Win.Log("Error: "+e.Data.ToString()); return ""; }
		}

	}
}
