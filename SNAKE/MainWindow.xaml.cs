﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;
using System.Data;

namespace SNAKE
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		DynamixelController dynamixelController;
		private Run run;
		Robot robot;
		public MainWindow()
		{
			InitializeComponent();
			//this.InitServos(13);
			for (int i = 0; i < 255; i++)
			{
				ComboBoxIDs.Items.Add(i);
			}
			ComboBoxCommands.Items.Add("Enable torque");
			ComboBoxCommands.Items.Add("Disable torque");
			ComboBoxCommands.Items.Add("Turn to angle");

			ComboBoxCommandsForAll.Items.Add("Enable torque");
			ComboBoxCommandsForAll.Items.Add("Disable torque");
			ComboBoxCommandsForAll.Items.Add("Write maximum torque");
			ComboBoxCommandsForAll.Items.Add("Turn all to 0 degrees");
			ComboBoxCommandsForAll.Items.Add("Ping all in robot");
			ComboBoxCommandsForAll.Items.Add("Ping all on protocol 1");
			ComboBoxCommandsForAll.Items.Add("Ping all on protocol 2");
			ComboBoxCommandsForAll.Items.Add("Lock in place");
			ComboBoxCommandsForAll.Items.Add("Lock both ends and end-effectors");
			ComboBoxCommandsForAll.Items.Add("Write all angle limits");

			dynamixelController = new DynamixelController("COM4", 1000000, this);//should the same DynamixelController be used in both MainWindow and Run?
			run = new Run(dynamixelController, this);
		}

		#region "Public functions"
		public void PostError(String e)
		{
			RichTextBoxErrors.AppendText(e + "\r");
		}

		public void Log(String s)
		{
			RichTextBoxMessages.AppendText(s + "\r");
		}
		#endregion

		#region "Private functions"
		private void Init()
		{
			List<Servo> servos = new List<Servo>() {
				new AX12A(1,dynamixelController,this)
				//new AX12A(1,dynamixelController,this),
			//	new AX12A(2,dynamixelController,this),
			//	new AX12A(3,dynamixelController,this)
				};
			robot = new Robot(servos, this);
		}

		private void ReadSaveAllServoPos()
		{
			Init();
			Log("trying to save positions");

			var servoList = this.robot.AllServos;

			DataSet dataSet = new DataSet("servoList") { Namespace = "NetFrameWork" };
			DataTable table = new DataTable()
			{
				TableName = "Servo Ids and Positions"
			};
			// create table columns
			DataColumn idColumn = new DataColumn("id", typeof(String));
			table.Columns.Add(idColumn);

			DataColumn positionColumn = new DataColumn("position", typeof(String));
			table.Columns.Add(positionColumn);

			dataSet.Tables.Add(table);

			Log("Servo List size is " + servoList.Count);

			foreach (var servo in servoList)
			{
				double angle = 0;
				DataRow newRow = table.NewRow();

				newRow["id"] = servo.ID.ToString();
				Log("servo " + servo.ID);

				servo.ReadAngle(ref angle);
				newRow["position"] = angle.ToString();
				Log("Pos " + angle);


				//dataSet.AcceptChanges();
				table.Rows.Add(newRow);

				//table.Add.newRow(newRow);
			}
			dataSet.AcceptChanges();
			//Console.WriteLine(dataSe)
			//dataSet.AcceptChanges();

			string json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
			Console.WriteLine("JSON -----------------");
			Console.WriteLine(json);

			FileRW writer = new FileRW(this);
			writer.Write2File("angles Table1", "", json);
		}

		private void LoadGoAllServoPos(string fileName, string path)
		{
			//initialize robot for testing
	 

			FileRW reader = new FileRW(this);

			string val = reader.ReadFromFile(fileName, path);


			Console.WriteLine("Val is " + val);

			DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(val);

			DataTable dataTable = dataSet.Tables["Servos"];
			//robot.ContainsServoWithID(1);

			foreach (DataRow row in dataTable.Rows)
			{
				byte id;
				Servo servo=null;
				byte temp=byte.Parse(row["id"].ToString());
				Log("ID IS " + temp.ToString());
				if ((Byte.TryParse(row["id"].ToString(),out id))){
				Log("Parse Correclty and ID is " + id.ToString());
					 
					robot.GetServoWithID(id,ref servo);
				}
				if(servo==null)
				{ Log("Error getting servo with id" + id); }

				else
				{
					double pos;
					if ((Double.TryParse(row["position"].ToString(), out pos)))
						servo.TurnToAngle(pos);
				}

				//row[ID].ToString;
				// code to move servos based on data in each row

			}
		}

		private void InitServos(int size)
		{
			for (int i = 0; i < size; i++)
			{
				StackPanel panel = new StackPanel() { Orientation = Orientation.Horizontal };
				CheckBox onlinebox = new CheckBox() { Content = "Online", FontSize = 20 };
				TextBlock id = new TextBlock() { Text = i.ToString(), FontSize = 20, FontWeight = FontWeights.Bold };

				//angle TextBox
				TextBox angleBox = new TextBox() { Tag = "AngleVal" + i, Width = 40 };
				//angle button
				Button setAngle = new Button()
				{
					Content = "Set Angle",
					Tag = "setAngle" + i
				};
				//setAngle.GetType().Name = setAngle.GetType().Name + i;
				panel.Children.Add(onlinebox);
				panel.Children.Add(id);
				panel.Children.Add(angleBox);
				panel.Children.Add(setAngle);

				ServoStack.Children.Add(panel);
				ServoStack.Children.Add(new Separator());

				Console.WriteLine("Tags = +" + setAngle.Tag + "   " + angleBox.Tag);

				//var item = items.Cast<Button>().FirstOrDefault(control => String.Equals(control.Tag, "setAngle0"));				
			}
		}

		private bool VerifyAngleAndID()
		{
			if (AngleBox.Text != "" && ID.Text != "")
				return true;
			else return false;
		}
		#endregion

		#region "Events"
		private void RichTextBoxMessages_TextChanged(object sender, TextChangedEventArgs e)
		{
			RichTextBoxMessages.ScrollToEnd();
		}

		private void RichTextBoxErrors_TextChanged(object sender, TextChangedEventArgs e)
		{
			RichTextBoxErrors.ScrollToEnd();
		}

		bool isConnected = false;
		private void ButtonConnectDynamixel_Click(object sender, RoutedEventArgs e)
		{
			if (!isConnected)
			{
				ButtonGo.IsEnabled = true;
				ButtonGoAll.IsEnabled = true;
				if (!dynamixelController.Connect())
					return;
				ButtonConnectDynamixel.Content = "Disconnect";
				this.isConnected = true;
			}
			else
			{
				ButtonGo.IsEnabled = false;
				ButtonGoAll.IsEnabled = false;
				dynamixelController.Disconnect();
				ButtonConnectDynamixel.Content = "Connect";
				this.isConnected = false;
			}
			// TODO Define the Columns

			// TODO Add the first text cell to the Grid
		}
		#endregion

		#region "Buttons"
		private void ButtonTurnToAngle_Click(object sender, RoutedEventArgs e)
		{
			if (VerifyAngleAndID())
				try
				{
					Byte id = Convert.ToByte(ID.Text);
					double angle = Convert.ToDouble(AngleBox.Text);
					Servo s = new AX12A(id, dynamixelController, this);
					dynamixelController.TurnToAngle(s, angle);
				}
				catch (Exception x)
				{
					PostError(x.Message);
				}
		}

		private void EnableTorque_Click(object sender, RoutedEventArgs e)
		{
			if (VerifyAngleAndID())
			{
				try
				{
					AX12A s = new AX12A(Convert.ToByte(ID.Text), dynamixelController, this);
					dynamixelController.EnableTorque(s);
				}
				catch (Exception x)
				{
					PostError(x.Message);
				}
			}
		}

		private void DisableTorque_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				AX12A s = new AX12A(Convert.ToByte(ID.Text), dynamixelController, this);
				dynamixelController.DisableTorque(s);
			}
			catch (Exception x)
			{
				PostError(x.Message);
			}
		}

		private void ButtonTest_Click(object sender, RoutedEventArgs e)
		{
			//if (dynamixelController.Ping(24, DynamixelController.PROTOCOL1))
			//	Log("Servo found");
			//else
			//	Log("Servo not found");


			//dynamixelController.PingAll(DynamixelController.PROTOCOL1);


			//Servo s = new AX12A(24, dynamixelController, this);
			//double a = 0;
			//if (dynamixelController.ReadAngle(s, ref a))
			//	Log("AX12-A with ID " + s.ID + " is at " + a + " degrees");
			//else
			//	PostError("Failed to read AX-12A angle");


			//List<Servo> servos = new List<Servo>();//add some servos here
			//Robot r = new Robot(servos);
			//List<double> a = new List<double>();
			//dynamixelController.ReadAllAngles(r, ref a);


			//List<byte> IDs = new List<byte> { 0, 1, 2, 3, 4, 5, 24, 25 };
			//List<double> angles = new List<double>();
			//dynamixelController.ReadAnglesFromAX12As(IDs, ref angles);


			//try
			//{
			//	Servo s = new AX12A(24, dynamixelController, this);
			//	s.WriteMaxTorque(Convert.ToDouble(TextBoxTest.Text));
			//	s.WriteTorqueLimit(Convert.ToDouble(TextBoxTest.Text));
			//	s.EnableTorque();
			//}
			//catch { PostError("Bad input in text box"); }


			//try
			//{
			//	Servo s = new AX12A(24, dynamixelController, this);
			//	s.WriteMaxTorque(30);
			//	s.WriteTorqueLimit(30);
			//	s.EnableTorque();
			//	s.TurnToAngle(Convert.ToDouble(TextBoxTest.Text));
			//}
			//catch { PostError("Bad input in text box"); }


			//try
			//{
			//	Servo s = new AX12A(24, dynamixelController, this);
			//	s.WriteSpeed(Convert.ToDouble(TextBoxTest.Text));
			//}
			//catch { PostError("Bad input in text box"); }


			//Servo s = new AX12A(24, dynamixelController, this);
			//s.WriteAngleLimits(-90,90);


			////This works, but remember that some values are spread over two registers and some registers are not used.
			//List<byte> bytesRead = new List<byte>();
			//dynamixelController.ReadRegisters(24, 1, 3, 9, ref bytesRead);
			//foreach (byte b in bytesRead)
			//{
			//	Log("Incoming bytes: " + b);
			//}


			//double z = dynamixelController.ReadVoltage(24, DynamixelController.PROTOCOL1);
			//Log("Voltage = " + z);


			//dynamixelController.CheckVoltageTimer(24, DynamixelController.PROTOCOL1, 0, 0, 3);


			//XL320 xl = new XL320(41, this.dynamixelController, this);
			//xl.TurnToAngle(-45);


			List<byte> IDs = new List<byte>();
			List<double> angles = new List<double>();
			run.robot.ReadAllServoAngles(ref IDs, ref angles);


			//WebSocketServer x = new WebSocketServer("localhost", "8008", this);


			//ReadSaveAllServoPos();
		}

		private void ButtonReadSaveAllServoPos_Click(object sender, RoutedEventArgs e)
		{
			this.Init();
			//	this.ReadSaveAllServoPos();
			//	this.LoadGoAllServoPos("angles Table1.json", @"c:\temp\");

			RobotRW rw = new RobotRW(this,ref robot);
			//rw.ReadSaveAllServoPositions("Servo", "");

			MovementSequence seq = new MovementSequence(this);
			rw.loadAllServoPositionsFromFile("Servo.json", @"c:\temp\", ref seq);

			seq.printSequence();
			//this.ReadSaveAllServoPos();
			this.LoadGoAllServoPos("angles Table1.json", @"c:\temp\");


			/*this.Log("reading servo");
		//	this.Init();
		//	this.ReadSaveAllServoPos();
			AX12A ax = new AX12A(1, -30, 30, dynamixelController, 0, this);

			double angle = 0;


			ax.ReadAngle(ref angle);
			//robot.AllServos[2].ReadAngle(ref angle);

			Log(angle.ToString());

			//this.readSaveAllServoPos();
	*/
		}

		private void ButtonClearMessages_Click(object sender, RoutedEventArgs e)
		{
			RichTextBoxMessages.Document.Blocks.Clear();
		}

		private void ButtonClearErrors_Click(object sender, RoutedEventArgs e)
		{
			RichTextBoxErrors.Document.Blocks.Clear();
		}

		private void ButtonGo_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				switch (ComboBoxCommands.SelectedItem.ToString())
				{
					case "Enable torque":
						Servo s0 = new AX12A(Convert.ToByte(ComboBoxIDs.SelectedItem.ToString()), dynamixelController, this);
						dynamixelController.EnableTorque(s0);
						break;
					case "Disable torque":
						Servo s1 = new AX12A(Convert.ToByte(ComboBoxIDs.SelectedItem.ToString()), dynamixelController, this);
						dynamixelController.DisableTorque(s1);
						break;
					case "Write maximum torque":
						Servo s2 = new AX12A(Convert.ToByte(ComboBoxIDs.SelectedItem.ToString()), dynamixelController, this);
						dynamixelController.WriteMaxTorque(s2, Convert.ToDouble(TetxBoxParameter1.Text));
						dynamixelController.WriteTorqueLimit(s2, Convert.ToDouble(TetxBoxParameter1.Text));
						break;
					case "Turn to angle":
						Servo s3 = new MX64AT(Convert.ToByte(ComboBoxIDs.SelectedItem.ToString()), dynamixelController, DynamixelController.PROTOCOL1, this);
						dynamixelController.TurnToAngle(s3, Convert.ToDouble(TetxBoxParameter0.Text));
						break;
					default:
						PostError("Error 041: Bad input in ComboBoxCommands");
						break;
				}
			}
			catch
			{
				PostError("Error 043: Bad input");
			}
		}

		private void ButtonGoAll_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				switch (ComboBoxCommandsForAll.SelectedItem.ToString())
				{
					case "Enable torque":
						this.run.robot.EnableTorque();
						break;
					case "Disable torque":
						this.run.robot.DisableTorque();
						break;
					case "Write maximum torque":
						this.run.robot.WriteMaximumTorque(Convert.ToDouble(TetxBoxParameter1.Text));
						break;
					case "Turn all to 0 degrees":
						this.run.robot.AllServosTo0Degrees();
						break;
					case "Ping all in robot":
						List<byte> servosFound = new List<byte>();
						List<byte> servosNotFound = new List<byte>();
						this.run.robot.PingAll(ref servosFound, ref servosNotFound);
						Log("Count of present servos: " + servosFound.Count);
						Log("Count of missing servos: " + servosNotFound.Count);
						break;
					case "Ping all on protocol 1":
						dynamixelController.PingAll(DynamixelController.PROTOCOL1);
						break;
					case "Ping all on protocol 2":
						dynamixelController.PingAll(DynamixelController.PROTOCOL2);
						break;
					case "Lock in place":
						this.run.robot.LockInPlace();
						break;
					case "Lock both ends and end-effectors":
						this.run.robot.LockPartOfRobotInPlace("End1Servos");
						this.run.robot.LockPartOfRobotInPlace("End2Servos");
						this.run.robot.LockPartOfRobotInPlace("End1EndEffectorServos");
						this.run.robot.LockPartOfRobotInPlace("End2EndEffectorServos");
						break;
					case "Write all angle limits":
						dynamixelController.WriteMultipleAngleLimits(run.robot.AllServos, run.angleLimitsCW_end1EndEffectorServos, run.angleLimitsCCW_end1EndEffectorServos);
						break;
					default:
						PostError("Error 041: Bad input in ComboBoxCommands");
						break;
				}
			}
			catch
			{
				PostError("Error 044: Bad input");
			}
		}
		#endregion
	}
}
