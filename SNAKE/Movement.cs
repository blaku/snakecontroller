﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNAKE
{
	/// <summary>
	/// Move servos listed in servoIDs to positions in goalPosition at speeds speeds. Movements should be chained for more advanced movements. 
	/// </summary>
	class Movement
	{
		 

		public byte servoID { get; set; }
		public double targetAngle { get; set; }
		public double speed { get; set; }

	 
		protected MainWindow MessageHandler { get; set; }


		
		public Movement(byte id, double angle, double speed)
		{
			this.servoID = id;
			this.targetAngle = angle;
			this.speed = speed;
		}
	}
}
