﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNAKE
{
	class MovementSequence
	{
		private List<Movement> movements = new List<Movement>();
		private List<int> delay = new List<int>();
		 
		private string name { get; set; }
		protected MainWindow MessageHandler { get; set; }



		public MovementSequence(MainWindow messageHandler)
		{
			MessageHandler = messageHandler;



		}

		public void addMovement(Movement movement,int delay)
		{
			this.movements.Add(movement);
			this.delay.Add(delay);
		}

		public bool getMovementSequence(ref List<Movement> movementList)
		{
			if(movements!=null)
			{ 
			movementList = this.movements;
				return true;
			}
			return false;
		}

		public int getMovementSequenceSize()
		{
			return this.movements.Count;
		}



		public int getDelayListSize()
		{
			return delay.Count;
		}
		public bool PlayMovementSequence(Robot robot)
		{

			return true;
			}



		public bool loadMovementSequenceFromFile()
			{




			return true;
			}


		public bool play()
		{
			if (movements != null || movements.Count > 0)
			{
				MessageHandler.Log("playing movement sequence " + this.name);



				return true;

				



			}

			return false;
		}


		public void printSequence()
		{
	 
			foreach (Movement movement in movements)
			{
				MessageHandler.Log("ID" + movement.servoID);
				MessageHandler.Log("Pos" + movement.targetAngle);
				MessageHandler.Log("Speed" + movement.speed);


			}


		}
		}

}
