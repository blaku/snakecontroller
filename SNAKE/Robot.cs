﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SNAKE
{
	/// <summary>
	/// This is a snake robot with a single finger on end1 and a parallel gripper on end2
	/// </summary>
	class Robot
	{
		public List<Servo> MiddleServos { get; } = new List<Servo>();
		public List<Servo> End1Servos { get; } = new List<Servo>();
		public List<Servo> End2Servos { get; } = new List<Servo>();
		public List<Servo> End1EndEffectorServos { get; } = new List<Servo>();
		public List<Servo> End2EndEffectorServos { get; } = new List<Servo>();
		public List<Servo> AllServos { get; } = new List<Servo>();
		private MainWindow MessageHandler { get; set; }

		public Robot(List<Servo> middleServos, List<Servo> end1Servos, List<Servo> end2Servos, List<Servo> end1EndEffectorServos, List<Servo> end2EndEffectorServos, MainWindow messageHandler)
		{
			MessageHandler = messageHandler;
			MiddleServos = middleServos;
			End1Servos = end1Servos;
			End2Servos = end2Servos;
			End1EndEffectorServos = end1EndEffectorServos;
			End2EndEffectorServos = end2EndEffectorServos;
			AllServos = middleServos.Concat(end1Servos.Concat(end2Servos.Concat(end1EndEffectorServos.Concat(end2EndEffectorServos)))).ToList();
		}

		public Robot(List<Servo> allServos, MainWindow messageHandler)
		{
			AllServos = allServos;
		}

		#region "Properties"
		private bool compliant = false;
		/// <summary>
		/// Set to true to make robot comply to externa forces. Set to false to disable the compliance. 
		/// </summary>
		public bool CompliantMode
		{
			get
			{
				return compliant;
			}
			set
			{
				if (value)
				{
					foreach (var s in AllServos)
					{
						if (s.CompliantModeActive)
							MessageHandler.PostError("Warning 001: Compliant mode already active");
						else
							s.CompliantModeActive = true;
					}
					compliant = true;
				}
				else
				{
					foreach (var s in AllServos)
					{
						if (!s.CompliantModeActive)
							MessageHandler.PostError("Warning 002: Compliant mode already inactive");
						else
							s.CompliantModeActive = false;
					}
					compliant = false;
				}
			}
		}
		#endregion

		#region "Public functions"
		public bool ContainsServoWithID(byte ID)
		{
			List<byte> IDs = new List<byte>();
			foreach (var s in AllServos)
			{
				IDs.Add(s.ID);
			}
			if (IDs.Contains(ID))
				return true;
			else
				return false;
		}

		/// <summary>
		/// Enables torque of all servos in robot
		/// </summary>
		/// <returns>False if at least one fails</returns>
		public bool EnableTorque()
		{
			bool success = true;
			foreach (var servo in AllServos)
			{
				if (!servo.EnableTorque())
					success = false;
			}
			return success;
		}

		/// <summary>
		/// Disables torque of all servos in robot
		/// </summary>
		/// <returns>False if at least one fails</returns>
		public bool DisableTorque()
		{
			bool success = true;
			foreach (var servo in AllServos)
			{
				if (!servo.DisableTorque())
					success = false;
			}
			return success;
		}

		/// <summary>
		/// Write a value in % to maximum torque and torque limit of all servos in robot
		/// </summary>
		/// <returns>False if at least one fails</returns>
		public bool WriteMaximumTorque(double torque)
		{
			bool success = true;
			foreach (var servo in AllServos)
			{
				if (!(servo.WriteMaxTorque(torque) && servo.WriteTorqueLimit(torque)))
					success = false;
			}
			return success;
		}

		public bool AllServosTo0Degrees()
		{
			bool success = true;
			foreach (var servo in AllServos)
			{
				if (!(servo.TurnToAngle(0)))
					success = false;
			}
			return success;
		}

		/// <summary>
		/// Reads all current IDs and angles of a robot
		/// </summary>
		/// <param name="servoIDsToreturn">A list to which the servo IDs will be added. The list will be cleared first. </param>
		/// <param name="anglesToReturn">A list to which the angles will be added. The list will be cleared first. </param>
		/// <returns></returns>
		public bool ReadAllServoAngles(ref List<byte> servoIDsToreturn, ref List<double> anglesToReturn)
		{
			servoIDsToreturn.Clear();
			anglesToReturn.Clear();
			bool success = true;
			foreach (var s in AllServos)
			{
				servoIDsToreturn.Add(s.ID);
				double a = 0;
				if (!s.ReadAngle(ref a))
				{
					success = false;
					continue;
				}
				anglesToReturn.Add(a);
			}
			return success;
		}

		/// <summary>
		/// Pings all servos in the robot
		/// </summary>
		/// <param name="servoIDsToreturn">A list to which the servo IDs will be added. The list will be cleared first. </param>
		/// <param name="anglesToReturn">A list to which the angles will be added. The list will be cleared first. </param>
		/// <returns></returns>
		public bool PingAll(ref List<byte> IDsFound, ref List<byte> IDsMissing)
		{
			IDsFound.Clear();
			IDsMissing.Clear();
			bool success = true;
			foreach (var s in AllServos)
			{
				if (!s.Ping())
				{
					IDsMissing.Add(s.ID);
					success = false;
					continue;
				}
				IDsFound.Add(s.ID);
			}
			return success;
		}

		/// <summary>
		/// Reads all angles and updates the Goal Positions to match them. Enables torque. 
		/// </summary>
		/// <returns>True if reading and writing all succeeded. False otherwise. </returns>
		public bool LockInPlace()
		{
			bool success = true;
			foreach (var s in AllServos)
			{
				double a = 0;
				if (!s.ReadAngle(ref a))
				{
					success = false;
					continue;
				}
				if (!s.TurnToAngle(a))
				{
					success = false;
					continue;
				}
			}
			if (!success)
				MessageHandler.PostError("Error 048: Failed to lock in place");
			return success;
		}

		/// <summary>
		/// Reads angles of given part and updates the Goal Positions to match them. Enables torque.  
		/// </summary>
		/// <param name="part">"MiddleServos", "End1Servos", "End2Servos", "End1EndEffectorServos", "End2EndEffectorServos", "BothEnds" or "BothEndEffectors"</param>
		/// <returns></returns>
		public bool LockPartOfRobotInPlace(String part)
		{
			List<Servo> servosToLock = new List<Servo>();
			switch (part)
			{
				case "MiddleServos":
					servosToLock = MiddleServos;
					break;
				case "End1Servos":
					servosToLock = MiddleServos;
					break;
				case "End2Servos":
					servosToLock = MiddleServos;
					break;
				case "End1EndEffectorServos":
					servosToLock = MiddleServos;
					break;
				case "End2EndEffectorServos":
					servosToLock = MiddleServos;
					break;
				case "BothEnds":
					servosToLock = MiddleServos;
					break;
				case "BothEndEffectors":
					servosToLock = MiddleServos;
					break;
				default:
					MessageHandler.PostError("Error 049: No such part to lock");
					return false;
			}

			bool success = true;
			foreach (var s in servosToLock)
			{
				double a = 0;
				if (!s.ReadAngle(ref a))
				{
					success = false;
					continue;
				}
				if (!s.TurnToAngle(a))
				{
					success = false;
					continue;
				}
			}
			if (!success)
				MessageHandler.PostError("Error 050: Failed to lock in place");
			return success;
		}
		
		///// <summary>
		///// Synch-reads present load of all servos
		///// </summary>
		///// <param name="presentLoads"></param>
		///// <returns></returns>
		//public bool readPresentLoads(ref List<double> presentLoads)
		//{
		//	List<byte> IDs = new List<byte>();
		//	foreach (var s in AllServos)
		//	{
		//		IDs.Add(s.ID);
		//	}

		//	if (!syncRead(IDs, AX12A.register_Present_Load_L, ref presentLoads))
		//	{
		//		Error.post("Error 008: Failed to read present loads");
		//		return false;
		//	}
		//	//TODO
		//	return true;
		//}

		public bool LoadRobotFromFile(String path, ref Robot robotResult)//TODO
		{
			List<Servo> middleServos = new List<Servo>(),
						end1Servos = new List<Servo>(),
						end2Servos = new List<Servo>(),
						end1EndEffectorServos = new List<Servo>(),
						end2EndEffectorServos = new List<Servo>();
			//if (!parseRobotFile(ref hmm))//TODO
			//{
			//	postError("Error 000: Failed to load robot file");
			//	return false;
			//}
			robotResult = new Robot(middleServos, end1Servos, end2Servos, end1EndEffectorServos, end2EndEffectorServos, MessageHandler);
			return false;
		}

		public bool GetServoWithID(byte ID, ref Servo refServo)
		{
			var servoList = this.AllServos;
			if (!(servoList.Count > 0))
				return false;

			foreach (var servo in servoList)
			{
				Console.WriteLine("Searching for servo with ID " + ID);
				if (servo.ID == ID)
				{
					refServo = servo;
					return true;
				}
			}
			return false;
		}
		#endregion

		#region "Private functions"
		/// <summary>
		/// Move a step to comply to external force. Call this using a timer. 
		/// </summary>
		/// <returns></returns>
		private bool PerformCompliantStep()
		{
			//TODO Check that port isn't busy
			foreach (var servo in AllServos)
			{
				double load = 0;
				if (!servo.ReadLoad(ref load))
				{
					MessageHandler.PostError("Error 004: Failed to read load for compliant step");
					return false;
				}
				double angle = 0;
				if (!servo.ReadAngle(ref angle))
				{//update angleError
					MessageHandler.PostError("Error 005: Failed to read angle for compliant step");
					return false;
				}
				if (load > servo.ComplianceLoadThreshold)
				{
					if (servo.AngularError > 0)
					{
						if (true)//TODO if(!move complianceStepAngle CCW at complianceSpeed)
						{
							MessageHandler.PostError("Error 006: Failed to take compliant step CCW");
							return false;
						}
					}
					else
					{
						if (true)//TODO if(!move complianceStepAngle CW at complianceSpeed)
						{
							MessageHandler.PostError("Error 007: Failed to take compliant step CW");
							return false;
						}
					}
				}
			}
			return true;
		}
		#endregion

		/*
			

			public bool closeGripper(byte speed, byte torque)
			{
				;
				//Close gripper until torque reached or completely closed
			}

			public bool setGripperPosition(double jawWidthInMillimeter, byte speed)
			{
				;
				//Close gripper until position reached (or max torque)
				return true;
			}

			public bool closeFinger(byte speed, byte torque)
			{
				;
				//Close finger until torque reached or completely closed
				return true;
			}

			//Enter negative diameter (mm) to be the finger the other way
			public bool moveFinger(int speed, byte torque, double toDiameter)
			{
				;
				//Close or open finger until diameter or torque reached
				return true;
			}

			public bool rotateFinger(int speed, byte torque, double toAngle)
			{
				;
				//Close or open finger until diameter or torque reached
				return true;
			}

			public bool rotateFingerToAngle(int speed, double toAngle)
			{
				;
				//Close or open finger until diameter (or default torque) reached
				return true;
			}

			public bool rotateFingerByAngle(int speed, byte torque, double toAngle)
			{
				;
				//Close or open finger until diameter or torque reached
				return true;
			}

			// Used for reading poses
			public bool readAngles(ref List<byte> IDs, ref List<byte> angles)
			{

			}

			public bool endEffectorTorque(bool enable, string end1_or_end2)
			{

			}
			
			public bool grabBottle(String end1Or2){
			  if(end1Or2=="end1"){
				;
			  }
			  elseif(end1Or2=="end2"){
				;
			  }
			  else
				return false;
			}

			public bool scratchHead(){
			  return false;
			}

			public bool carryBag (){
			  return false;
			}

			public bool moveToNeckPosition (){
			  ;
			  return false;
			}

			public bool braceUnderArm(String end1Or2){
			  if(end1Or2=="end1"){
				;
			  }
			  elseif(end1Or2=="end2"){
				;
			  }
			  else
				return false;
			}
		*/
	}
}
