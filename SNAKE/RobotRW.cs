﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// this class reads and writes servo properties from files
namespace SNAKE
{
	/// <summary>
	/// This class reads and writes servo properties from files
	/// </summary>
	class RobotRW
	{
		private MainWindow win { get; set; }
		private Robot robot { get; set; }
		private FileRW RW { get; set; }
		public RobotRW(MainWindow Window, ref Robot Robot)
		{
			win = Window;
			this.robot = Robot;
			RW = new FileRW(win);
		}

		public void ReadSaveAllServoPositions(string FileName, string path)
		{
			win.Log("trying to read all servo positions");
		 

			var servoList = this.robot.AllServos;
		 

			DataSet dataSet = new DataSet("servoList") { Namespace = "NetFrameWork" };
			DataTable table = new DataTable() { TableName = "Servo Ids and Positions" };

			// create table columns
			DataColumn idColumn = new DataColumn("id", typeof(String));
			table.Columns.Add(idColumn);

			DataColumn positionColumn = new DataColumn("position", typeof(String));
			table.Columns.Add(positionColumn);

			DataColumn speedColumn= new DataColumn("speed", typeof(String));
			table.Columns.Add(speedColumn);

			dataSet.Tables.Add(table);

			win.Log("Servo List size is " + servoList.Count);

			foreach (var servo in servoList)
			{
				double angle = 0;
				double speed = 0;

				DataRow newRow = table.NewRow();

				newRow["id"] = servo.ID.ToString();
				win.Log("Servo " + servo.ID);

				servo.ReadAngle(ref angle);
				newRow["position"] = angle.ToString();
				win.Log("Position " + angle);


				servo.ReadSpeed(ref speed);
				newRow["speed"] = angle.ToString();
				win.Log("Speed " + speed.ToString());

				//dataSet.AcceptChanges();
				table.Rows.Add(newRow);

				//table.Add.newRow(newRow);
			}

			dataSet.AcceptChanges();
			//Console.WriteLine(dataSe)
			//dataSet.AcceptChanges();

			string json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);

			win.Log("Writing " + FileName + " to " + path);

			//for debugging
			Console.WriteLine(json);



			RW.Write2File(FileName, path, json);

		}


		public void LoadGoAllServoPos(string fileName, string path)
		{
			string val = this.RW.ReadFromFile(fileName, path);


			Console.WriteLine("Val is " + val);

			DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(val);

			DataTable dataTable = dataSet.Tables["Servos"];
            //robot.ContainsServoWithID(1);

            foreach (DataRow row in dataTable.Rows)
            {
                byte id;
                Servo servo = null;
                byte temp = byte.Parse(row["id"].ToString());
                win.Log("ID IS " + temp.ToString());
                if ((byte.TryParse(row["id"].ToString(), out id)))
                {
                    win.Log("Parse Correclty and ID is " + id.ToString());

                    this.robot.GetServoWithID(id, ref servo);
                }
                if (servo == null)
                { win.Log("Error getting servo with id" + id); }

                else
                {
                    double pos;
                    if ((Double.TryParse(row["position"].ToString(), out pos)))
                        servo.TurnToAngle(pos);
                }

                //row[ID].ToString;
                // code to move servos based on data in each row

            }
        }



        public void loadAllServoPositionsFromFile(string fileName, string path, ref MovementSequence movementSequence)
		{
			string val = RW.ReadFromFile(fileName, path);

			byte id;
			double angle;
			double speed;

			Console.WriteLine("Val is " + val);

			DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(val);

			DataTable dataTable = dataSet.Tables["Servos"];
			//robot.ContainsServoWithID(1);

			foreach (DataRow row in dataTable.Rows)
			{
				

				byte temp = byte.Parse(row["id"].ToString());

				win.Log("ID IS " + temp.ToString());

				if ((Byte.TryParse(row["id"].ToString(), out id)))
				{
					win.Log("Parse Correclty and ID is " + id.ToString());


				}
				else { win.Log("Error Parsing ID"); }
				
				
					
				if ((Double.TryParse(row["position"].ToString(), out angle)))
					{
						win.Log("Parsed Correclty << (I kept this for you) and pos is " + id.ToString());
					}
				else { win.Log("Error Parsing position"); }


				if ((Double.TryParse(row["speed"].ToString(), out speed)))
				{
					win.Log("Parsed Correclty << (I kept this for you) and speed is " + id.ToString());
				}
				else { win.Log("Error Parsing position"); }


				movementSequence.addMovement(new Movement(id, angle, speed),1);

			}


				//row[ID].ToString;
				// code to move servos based on data in each row

			}
		}



	}




