﻿using System.Collections.Generic;

namespace SNAKE
{
	class Run
	{
		public Robot robot;
		private MainWindow MessageHandler { get; set; }
		public List<double> angleLimitsCW_end1EndEffectorServos = new List<double>();
		public List<double> angleLimitsCCW_end1EndEffectorServos = new List<double>();

		public Run(DynamixelController dynamixelController, MainWindow messageHandler)
		{
			MessageHandler = messageHandler;
		/*	List<Servo> middleServos = new List<Servo>() {
				new AX12A(11, dynamixelController, MessageHandler),
				new AX12A(12, dynamixelController, MessageHandler),
				new AX12A(13, dynamixelController, MessageHandler),
				new AX12A(14, dynamixelController, MessageHandler),
				new AX12A(15, dynamixelController, MessageHandler),
				new AX12A(16, dynamixelController, MessageHandler),
				new AX12A(17, dynamixelController, MessageHandler),
				new MX64AT(18, dynamixelController, DynamixelController.PROTOCOL1, MessageHandler)
			};
			List<Servo> end1Servos = new List<Servo>() {
				new AX12A(21, dynamixelController, MessageHandler),
				new AX12A(22, dynamixelController, MessageHandler),
				new AX12A(23, dynamixelController, MessageHandler)
			};
			List<Servo> end2Servos = new List<Servo>() {
				new AX12A(31, dynamixelController, MessageHandler),
				new AX12A(32, dynamixelController, MessageHandler),
				new AX12A(33, dynamixelController, MessageHandler),
				new AX12A(34, dynamixelController, MessageHandler)
			};

			//One-finger version
			//List<Servo> end1EndEffectorServos = new List<Servo>() {
			//	new XL320(41, dynamixelController, MessageHandler),
			//	new XL320(42, dynamixelController, MessageHandler),
			//	new XL320(43, dynamixelController, MessageHandler)
			//};

			//Two-finger version
			List<Servo> end1EndEffectorServos = new List<Servo>() {
				//Finger 1
				new XL320(41, dynamixelController, MessageHandler),
				new XL320(42, dynamixelController, MessageHandler),
				new XL320(43, dynamixelController, MessageHandler),
				new XL320(44, dynamixelController, MessageHandler),
				//Finger 2
				new XL320(45, dynamixelController, MessageHandler),
				new XL320(46, dynamixelController, MessageHandler),
				new XL320(47, dynamixelController, MessageHandler),
				new XL320(48, dynamixelController, MessageHandler)
			};
			angleLimitsCW_end1EndEffectorServos = new List<double> { 0, 165, 165, 167, 0, 179, 175, 179 };
			angleLimitsCCW_end1EndEffectorServos = new List<double> { 1023, 818, 830, 850, 1023, 835, 845, 855 };
			for (int i=0;i<angleLimitsCW_end1EndEffectorServos.Count;i++)
				angleLimitsCW_end1EndEffectorServos[i] = DynamixelController.Angle_ushortToDegrees((ushort)angleLimitsCW_end1EndEffectorServos[i], typeof(XL320),MessageHandler);
			for (int i = 0; i < angleLimitsCCW_end1EndEffectorServos.Count; i++)
				angleLimitsCCW_end1EndEffectorServos[i] = DynamixelController.Angle_ushortToDegrees((ushort)angleLimitsCCW_end1EndEffectorServos[i], typeof(XL320), MessageHandler);



			/*
			Servo ID    Angle limits
			41			0		 1023
			42			165		 818
			43			165		 830
			44			167		 850

			45			0		 1023		(512->0 = 1023->0 = 1023->512)
			46			179		 835
			47			175		 845
			48			179		 855
			*/

			List<Servo> end2EndEffectorServos = new List<Servo>() {
				new AX12A(51, dynamixelController, MessageHandler)
			};
			//robot = new Robot(middleServos, end1Servos, end2Servos, end1EndEffectorServos, end2EndEffectorServos, messageHandler);
		//	robot = new Robot(end1EndEffectorServos, messageHandler);
			//dynamixelController.CheckVoltageTimer(11, DynamixelController.PROTOCOL1, 0, 0, 3);//TODO crashed if not conencted?

	 
	//TODO use the robot

		
		}

		 





	}
}
