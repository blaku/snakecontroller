﻿using System.Collections.Generic;

namespace SNAKE
{
	/// <summary>
	/// A sequence of movements. Contains a list of Movement objects and a list of instants of time when each movement should be executed. 
	/// </summary>
	class Sequence
	{
		public List<Movement> Movements { get; set; }
		public List<double> MovementIntervals { get; set; }
		private MainWindow MessageHandler { get; set; }

		public Sequence(List<Movement> movements, List<double> movementIntervals, MainWindow messageHandler)
		{
			MessageHandler = messageHandler;
			if (!(movements.Count == movementIntervals.Count))
			{
				messageHandler.PostError("Error 051: Counts of movements and movementIntervals don't match");
				return;
			}
			Movements = movements;
			MovementIntervals = movementIntervals;
		}

		public bool PlaySequence()
		{
			System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
			for (int i = 0; i < Movements.Count; i++)
			{
                if (!Movements[i].PlayMovement(false))
				{
					MessageHandler.PostError("Error 053: Failed to play a movement in the sequence");
					return false;
				}
				stopWatch.Restart();
				while (stopWatch.ElapsedMilliseconds < MovementIntervals[i])
					System.Threading.Thread.Sleep(10);//TODO will this prevent the GUI from locking?
                   
            }

            return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="speedFactor">Factor for multiplying the speed of each movement and dividing the movement intervals</param>
		/// <returns></returns>
		public bool PlaySequenceAtModifiedSpeed(double speedFactor)
		{
			System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
			for (int i = 0; i < Movements.Count; i++)
			{
                /*
                if (!Movements[i].PlayMovementAtModifiedSpeed(false, speedFactor))
				{
					MessageHandler.PostError("Error 060: Failed to play a movement in the sequence");
					return false;
				}
                */
				stopWatch.Restart();
				while (stopWatch.ElapsedMilliseconds < MovementIntervals[i]/speedFactor)
					System.Threading.Thread.Sleep(10);//TODO will this prevent the GUI from locking?
			}
			return true;
		}



		////Interpolate between close steps to use fewer steps
		//public bool approximateSequence(Sequence rawSequence, ref Sequence returnedSequence, double stepsPerSecond)
		//{
		//	returnedSequence.clear();
		//	returnedSequence.add(0, rawSequence[0]);
		//	double period = 1 / stepsPerSecond;
		//	double lastTime = 0
		//  for (int i = 1; i < rawSequence.length; i++)
		//	{
		//		Sequence part = new Sequence();
		//		for (double t = lastTime; t - lastTime < period; t = rawSequence[i].Time{
		//			part.add(rawSequence.[i));
		//			i++;
		//		}
		//		Movement m = new Movement();
		//		double timeOfMovement = 0;
		//		if (!ApproximatePartOfSequence(part, ref m, ref timeOfMovement))
		//		{
		//			//messageHandler("Error 000: Failed to approxi
		//			return false;
		//		}
		//		returnedSequence.add(m, timeOfMovement);
		//	}
		//}

		//public bool ElementAt(int index, ref Movement movementToReturn)
		//{
		//	if (index < 0 || index >= this.length)
		//	{
		//		messageHandler.PostError("Error 000: Bad index of movement";
		//		return false;
		//	}
		//	movementToReturn = this.movements[index];
		//	return true;
		//}
	}
}
