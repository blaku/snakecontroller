﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace SNAKE
{
	class WebSocketClient
	{

		public void run(string[] args)
		{
			using (var ws = new WebSocket("ws://localhost:8008"))
			{
				ws.OnMessage += (sender, e) =>
					Console.WriteLine("Laputa says: " + e.Data);

				ws.Connect();
				ws.Send("BALUS");
				Console.ReadKey(true);
			}
		}
	}
}



