﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;

namespace SNAKE
{

	class WebSocketServer : WebSocketBehavior
	{
		public WebSocketSharp.Server.WebSocketServer wssv;
		MainWindow win;
		// ip should be with https identifiers and all /
		public WebSocketServer() { }

		public WebSocketServer(string ip, string port, MainWindow win)
		{
			// wssv = new WebSocketSharp.Server.WebSocketServer("localhost"+":"+port);
			// wssv = new WebSocketSharp.Server.WebSocketServer("ws://" + "localhost" + ":" + port);
			wssv = new WebSocketSharp.Server.WebSocketServer("ws://localhost");
			wssv.AddWebSocketService<WebSocketServer>("/Control");

			wssv.Start();

			this.win = win;
			win.Log("Server has started on " + ip + ":" + port);
		}

		protected override void OnMessage(MessageEventArgs e)
		{
			this.MsgHandler(e);
			var msg = e.Data;

			Send(msg);
		}

		public void MsgHandler(MessageEventArgs msg)
		{
			// message handling code

			win.Log("Server recieved: " + msg.Data);
			
			// create packet encapsulator
			AnonPacket packet = JsonConvert.DeserializeObject<AnonPacket>(msg.Data);
			win.Log("Packet type is: " + packet.type);


			if (packet.type.Equals("control"))
			{
				// control code

			}

			if (packet.type.Equals("feedback"))
			{
				// feedback code

			}

		}
	}
}

