﻿namespace SNAKE
{
	class XL320 : Servo
	{
		//Registers in EEPROM												
		public const byte register_Model_Number_L = 0;
		public const byte register_Model_Number_H = 1;
		public const byte register_Version_of_Firmware = 2;
		public const byte register_ID = 3;
		public const byte register_Baud_Rate = 4;
		public const byte register_Return_Delay_Time = 5;
		public const byte register_CW_Angle_Limit_L = 6;
		public const byte register_CW_Angle_Limit_H = 7;
		public const byte register_CCW_Angle_Limit_L = 8;
		public const byte register_CCW_Angle_Limit_H = 9;
		public const byte register_Control_Mode = 11;
		public const byte register_Limit_Temperature = 12;
		public const byte register_Lower_Limit_Voltage = 13;
		public const byte register_Upper_Limit_Voltage = 14;
		public const byte register_Max_Torque_L = 15;
		public const byte register_Max_Torque_H = 16;
		public const byte register_Return_Level = 17;
		public const byte register_Alarm_Shutdown = 18;

		//Registers in RAM
		public const byte register_Torque_Enable = 24;
		public const byte register_LED = 25;
		public const byte register_D_Gain = 27;
		public const byte register_I_Gain = 28;
		public const byte register_P_Gain = 29;
		public const byte register_Goal_Position_L = 30;
		public const byte register_Goal_Position_H = 31;
		public const byte register_Goal_Velocity_L = 32;
		public const byte register_Goal_Velocity_H = 33;
		public const byte register_Goal_Torque_L = 35;//same as register_Torque_Limit_L for the other servos
		public const byte register_Goal_Torque_H = 36;//same as register_Torque_Limit_H for the other servos
		public const byte register_Present_Position_L = 37;
		public const byte register_Present_Position_H = 38;
		public const byte register_Present_Speed_L = 39;
		public const byte register_Present_Speed_H = 40;
		public const byte register_Present_Load_L = 42;
		public const byte register_Present_Load_H = 42;
		public const byte register_Present_Voltage = 45;
		public const byte register_Present_Temperature = 46;
		public const byte register_Registered_Instruction = 47;
		public const byte register_Moving = 49;
		public const byte register_Hardware_Error_Status = 50;
		public const byte register_Punch_L = 51;
		public const byte register_Punch_H = 52;
		
		public const string servoType = "XL-320";
		public const int protocolVersion = 2;

		/// <summary>
		/// A Dynamixel XL-320 servo
		/// </summary>
		/// <param name="servoID">[0..252]</param>
		public XL320(byte servoID, DynamixelController controller, MainWindow messageHandler)
			: base(servoID, controller, messageHandler, protocolVersion, servoType)
		{
			//No init necessary
		}
	}
}

