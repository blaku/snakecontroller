﻿namespace SNAKE
{
	abstract class Servo
	{
		protected DynamixelController dynamixelController;

		#region "Properties"
		protected MainWindow MessageHandler { get; set; }
		protected string ServoType { get; set; }
		protected double TargetAngle { get; set; } = 0;
		public bool CompliantModeActive { get; set; } = false;

		protected byte id = 1;
		/// <summary>
		/// Servo address [0..253] for protocol 1, [0..252] for protocol 2. 254 is broadcast address and cannot be set here. 
		/// </summary>
		public byte ID
		{
			get { return this.id; }
			set
			{
				if (ProtocolVersion == 1 && value >= 0 && value < 254)
					this.id = value;
				else if (ProtocolVersion == 2 && value >= 0 && value < 253)
					this.id = value;
				else
					MessageHandler.PostError("Error 037: Failed to set ID");
			}
		}

		private double complianceLoadThreshold = 20;
		/// <summary>
		/// Load applied in % before the arm complies to external force if CompliantModeActive
		/// </summary>
		public double ComplianceLoadThreshold
		{
			get { return this.complianceLoadThreshold; }
			set
			{
				if (value >= 0 && value <= 100)
					this.complianceLoadThreshold = value;
				else
					MessageHandler.PostError("Error 016: ComplianceLoadThreshold not in %");
			}
		}

		private double angularError = 0;
		/// <summary>
		/// The difference between the current angle and the setpoint. Updated when the current angle is read.
		/// </summary>
		public double AngularError
		{
			get { return this.angularError; }
			set
			{
				if (value <= 150 && value >= -150)
					this.angularError = value;
				else
					MessageHandler.PostError("Error 017: angularError out of range");
			}
		}

		private double angularErrorThreshold = 0.5;
		/// <summary>
		/// Degrees of offset from setpoint before the arm complies to external force if CompliantModeActive
		/// </summary>
		public double AngularErrorThreshold
		{
			get { return this.angularErrorThreshold; }
			set
			{
				if (value >= 0 && value <= 300)
					this.angularErrorThreshold = value;
				else
					MessageHandler.PostError("Error 022: AngleErrorThreshold out of range");
			}
		}

		private double cwAngleLimit;
		/// <summary>
		/// In degrees, where -150 is fully CCW, and 150 is fully CW
		/// </summary>
		public double CW_AngleLimit
		{
			get { return this.cwAngleLimit; }
			set
			{
				if (value >= -150 && value <= 150)
					this.cwAngleLimit = value;
				else
					MessageHandler.PostError("Error 014: CW_AngleLimit out of range");
			}
		}

		private double ccwAngleLimit;
		/// <summary>
		/// In degrees, where -150 is fully CCW, and 150 is fully CW
		/// </summary>
		public double CCW_AngleLimit
		{
			get { return this.ccwAngleLimit; }
			set
			{
				if (value >= -150 && value <= 150)
					this.ccwAngleLimit = value;
				else
					MessageHandler.PostError("Error 019: CCW_AngleLimit out of range");
			}
		}

		private int protocolVersion = 1;
		/// <summary>
		/// Dynamixel protocol 1 or 2. AX-12A is compatible with 1, MX-64AT is compatible with either, depending on FW, XL-320 is compatible with 2.
		/// </summary>
		public int ProtocolVersion
		{
			get { return this.protocolVersion; }
			set
			{
				if (value == 1 || value == 2)
					this.protocolVersion = value;
				else
					MessageHandler.PostError("Error 018: Bad protocol version");
			}
		}

		/// <summary>
		/// Returns true if AngularError > AngularErrorThreshold
		/// </summary>
		public bool IsMoving
		{
			get
			{
				if (AngularError > AngularErrorThreshold)
					return true;
				return false;
			}
		}
		#endregion

		public Servo(byte servoID, DynamixelController controller, MainWindow messageHandler, int protocolVersion, string servoType)
		{
			ProtocolVersion = protocolVersion;
			MessageHandler = messageHandler;
			ID = servoID;//has to be after ProtocolVersion and MessageHandler
			dynamixelController = controller;
			ServoType = servoType;
		}

		#region "Public functions"
		public void Init(double CWAngleLimit, double CWWAngleLimit)//TODO
		{
			this.WriteAngleLimits(CWAngleLimit, CWWAngleLimit);
			// set servo speed
			// set torques
			// set other safety features 
		}

		/// <summary>
		/// Reads the current angle and updates the AngleError
		/// </summary>
		/// <param name="angleToReturn">[-150..150] degrees</param>
		/// <returns></returns>
		public bool ReadAngle(ref double angleToReturn)
		{
			if (!dynamixelController.ReadAngle(this, ref angleToReturn))
			{
				MessageHandler.PostError("Error 031: Failed to read angle");
				return false;
			}
			AngularError = TargetAngle - angleToReturn;
			return true;
		}

		/// <summary>
		/// Turn servo to an angle in degrees
		/// </summary>
		/// <param name="angle">In degrees [-150..150]</param>
		/// <returns></returns>
		public bool TurnToAngle(double angle)
		{
			if (!dynamixelController.TurnToAngle(this, angle))
			{
				MessageHandler.PostError("Error 023: Failed to go to angle");
				return false;
			}
			return true;
		}

		public bool WriteAngleLimits(double CWLimit, double CCWLimit)
		{
			if (!dynamixelController.WriteCW_AngleLimit(this, CWLimit))
			{
				MessageHandler.PostError("Error 025: Failed to set CW angle limit");
				return false;
			}
			if (!dynamixelController.WriteCCW_AngleLimit(this, CCWLimit))
			{
				MessageHandler.PostError("Error 026: Failed to set CCW angle limit");
				return false;
			}
			MessageHandler.Log("Wrote the angle limits [" + CWLimit + ".." + CCWLimit + "] to servo " + ID);
			return true;
		}

		/// <summary>
		/// The maximum amount of torque before the Overload Error is thrown and Alarm Shutdown is triggered by default
		/// </summary>
		/// <param name="torquePercentage"></param>
		/// <returns></returns>
		public bool WriteMaxTorque(double torquePercentage)
		{
			if (!dynamixelController.WriteMaxTorque(this, torquePercentage))
			{
				MessageHandler.PostError("Error 032: Failed to write max torque");
				return false;
			}
			MessageHandler.Log("Wrote max torque " + torquePercentage + " % for servo " + ID);
			return true;
		}

		/// <summary>
		/// The torque limit is reset to Max Torque when power is cylced. 
		/// The torque limit is set to 0 when Alarm Shutdown is triggered. Set it to something else to return the servo from the shutdown.  
		/// </summary>
		/// <param name="torquePercentage">Maximum torque before triggering alarm. [0..100%] You e.g. need at least 6 % before AX12-A moves. </param>
		/// <returns></returns>
		public bool WriteTorqueLimit(double torquePercentage)
		{
			if (!dynamixelController.WriteTorqueLimit(this, torquePercentage))
			{
				MessageHandler.PostError("Error 033: Failed to write torque limit");
				return false;
			}
			MessageHandler.Log("Wrote torque limit " + torquePercentage + " % for servo " + ID);
			return true;
		}

		public bool EnableTorque()
		{
			if (!dynamixelController.EnableTorque(this))
			{
				MessageHandler.PostError("Error 034: Failed to enable torque");
				return false;
			}
			MessageHandler.Log("Enabled torque on servo " + ID);
			return true;
		}

		public bool DisableTorque()
		{
			if (!dynamixelController.DisableTorque(this))
			{
				MessageHandler.PostError("Error 035: Failed to disable torque");
				return false;
			}
			MessageHandler.Log("Disabled torque on servo " + ID);
			return true;
		}


		/// <summary>
		/// Read Present Speed in % of maximum. Negative if CCW, positive if CW
		/// </summary>
		/// <param name="speed">% of maximum, negative if CCW, positive if CW</param>
		public bool ReadSpeed(ref double speedPercentage)
		{
			if (!dynamixelController.ReadSpeed(this, ref speedPercentage))
			{
				MessageHandler.PostError("Error 039: Failed to read speed");
				return false;
			}
			MessageHandler.Log("Servo " + ID + " runs at speed " + speedPercentage + " %");
			return true;
		}


        /* this method is duplicated
         
		/// <summary>
		/// Read Present Speed in % of maximum. Negative if CCW, positive if CW
		/// </summary>
		/// <param name="speed">% of maximum, negative if CCW, positive if CW</param>
		public bool ReadSpeed(ref double speedPercentage)
		{
			if (!dynamixelController.ReadSpeed(this, ref speedPercentage))
			{
				MessageHandler.PostError("Error 039: Failed to read speed");
				return false;
			}
			MessageHandler.Log("Servo " + ID + " runs at speed " + speedPercentage + " %");
			return true;
		}
        */

		/// <summary>
		/// Write Moving Speed (Goal Velocity) in % of maximum
		/// </summary>
		/// <param name="speed">% of maximum</param>
		public bool WriteSpeed(double speedPercentage)
		{
			if (!dynamixelController.WriteSpeed(this, speedPercentage))
			{
				MessageHandler.PostError("Error 036: Failed to write speed");
				return false;
			}
			MessageHandler.Log("Wrote speed " + speedPercentage + " % for servo " + ID);
			return true;
		}

		/// <summary>
		/// Reads the current load in percent. Negative if CCW and positive if CW. 
		/// </summary>
		/// <param name="loadToReturn">[-100..100] %</param>
		/// <returns></returns>
		public bool ReadLoad(ref double loadToReturn)//TODO test
		{
			if (!dynamixelController.ReadLoad(this, ref loadToReturn))
			{
				MessageHandler.PostError("Error 038: Failed to read load");
				return false;
			}
			return true;
		}

		public bool CheckIfMoving(ref bool moving)
		{
			double speedPercentage = 0;
			if (!ReadSpeed(ref speedPercentage))
			{
				//moving=?;//TODO
				return false;
			}
			if (speedPercentage > 1 || speedPercentage < -1)//TODO increase precision by changing Map to double
			{
				MessageHandler.Log("Servo " + ID + " runs at speed " + speedPercentage + " %");
				moving = true;
			}
			return true;
		}

		public bool Ping()
		{
			return dynamixelController.Ping(this.ID, this.protocolVersion);
		}
		#endregion
	}
}
